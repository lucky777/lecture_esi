#!/usr/bin/python3
#coding:utf-8

from tkinter import *
from tkinter import messagebox
from PIL import ImageTk, Image

global login
login = "a"
global pwd
pwd = "a"
global connected
connected = False

win = Tk()

xLeft = int(win.winfo_screenwidth()//2 - 500)
yTop = int(win.winfo_screenheight()//2 - 500)
win.geometry(f"800x600+{xLeft}+{yTop}")
win.resizable(False, False)
#win.iconbitmap("D:\Etudes\Ecole\TFE\TFE_Biblio\Code\Blacky-Book2\Blacky-Book\BookSeel.ico")
win.title("Lecture ESI")

def acceuil():
    global frame0
    frame0 = Frame(win)
    global frame1
    frame1 = Frame(win)
    global frame2
    frame2  = LabelFrame(win, text="Tendances", pady=14, padx=14)
    global frame3
    frame3  = Frame(win)

    if connected:
        img_canvas = Canvas(frame0, height=50, width=50)
        img_profile = Image.open("img/empty_profile.png")
        img_canvas.image = ImageTk.PhotoImage(img_profile.resize((42, 42), Image.ANTIALIAS))
        img_canvas.create_image(27, 30, image=img_canvas.image)
        label_profile = Label(frame0, text=login)

    button_con = Button(frame0, text=("Profil" if connected else "Connexion"), command=lambda: con())

    entry_search = Entry(frame1, width=30)
    button_search = Button(frame1, text = "Chercher", command=lambda: search())

    button_left = Button(frame2, text="<", state=DISABLED)
    canvas_trends = Canvas(frame2, height=50, width=100, bg="#fff")
    button_right = Button(frame2, text=">", state=DISABLED)

    label_about = Label(frame3, text="Lecture ESI\nversion alpha\nby lucky777")

    frame0.pack(pady=(0, (0 if connected else 77)))
    frame1.pack(pady=25)
    frame2.pack(padx=50, pady=50)
    frame3.pack(padx=50, pady=10)

    if connected:
        img_canvas.grid()
        label_profile.grid(row=1)
    button_con.grid(row=2, padx=680, pady=(7 if connected else 21))

    entry_search.grid()
    button_search.grid(row=0, column=1, padx=15)

    button_left.grid()
    canvas_trends.grid(row=0, column=1, ipadx=200, ipady=40)
    button_right.grid(row=0, column=2)

    label_about.grid()

def profile():
    pass

def search():
    messagebox.showinfo("OK", "ok bro!")

def con():
    win_con = Toplevel()
    xLeft = int(win_con.winfo_screenwidth()//2 - 300)
    yTop = int(win_con.winfo_screenheight()//2 - 500)
    win_con.geometry(f"500x400+{xLeft}+{yTop}")
    win_con.resizable(False, False)
    win_con.title("Connexion")

    cframe1 = LabelFrame(win_con, text="connexion", pady=7, padx=7)
    cframe2 = Frame(win_con)

    label_login = Label(cframe1, text="Pseudo")
    entry_login = Entry(cframe1, width=30)
    label_pwd = Label(cframe1, text="Mot de passe")
    entry_pwd = Entry(cframe1, width=30, show='*')

    button_con = Button(cframe2, text="Se connecter", command=lambda: connect())
    button_new = Button(cframe2, text="Créer compte", command=lambda: new())

    cframe1.pack(pady=(100, 10))
    cframe2.pack()

    label_login.grid(pady=7)
    entry_login.grid(row=0, column=1, pady=7)
    label_pwd.grid(row=1, column=0, pady=7)
    entry_pwd.grid(row=1, column=1, pady=7)

    button_con.grid(pady=10)
    button_new.grid(row=1, column=0)

    def new():
        author = BooleanVar()

        cframe1.destroy()
        cframe2.destroy()

        nframe3 = LabelFrame(win_con, text="infos", padx=7, pady=4)
        nframe4 = LabelFrame(win_con, text="auteur", padx=7, pady=4)
        nframe5 = Frame(win_con)

        label_new_login = Label(nframe3, text="Pseudo")
        entry_new_login = Entry(nframe3, width=30)
        label_new_mail = Label(nframe3, text="Mail")
        entry_new_mail = Entry(nframe3, width=30)
        label_new_pwd = Label(nframe3, text="Mot de passe")
        entry_new_pwd = Entry(nframe3, width=30, show='*')
        label_new_pwd_conf = Label(nframe3, text="Confirmer mot de passe")
        entry_new_pwd_conf = Entry(nframe3, width=30, show='*')

        checkbox_author = Checkbutton(nframe4, text="Je suis un auteur", variable=author, onvalue=True, offvalue=False, command=lambda: check_author())

        label_author_lastname = Label(nframe4, text="Nom", state=DISABLED)
        entry_author_lastname = Entry(nframe4, width=30, state=DISABLED)
        label_author_firstname = Label(nframe4, text="Prenom", state=DISABLED)
        entry_author_firstname = Entry(nframe4, width=30, state=DISABLED)

        button_create = Button(nframe5, text="Créer", command=lambda: create())

        nframe3.pack(pady=(15, 20))
        nframe4.pack()
        nframe5.pack()

        label_new_login.grid(row=0, column=0, pady=7)
        entry_new_login.grid(row=0, column=1, pady=7)
        label_new_mail.grid(row=1, column=0, pady=7)
        entry_new_mail.grid(row=1, column=1, pady=7)
        label_new_pwd.grid(row=2, column=0, pady=7)
        entry_new_pwd.grid(row=2, column=1, pady=7)
        label_new_pwd_conf.grid(row=3, column=0, pady=7)
        entry_new_pwd_conf.grid(row=3, column=1, pady=7)

        checkbox_author.grid(row=0, column=0)

        label_author_lastname.grid(row=1, column=0, pady=7)
        entry_author_lastname.grid(row=1, column=1, pady=7)
        label_author_firstname.grid(row=2, column=0, pady=7)
        entry_author_firstname.grid(row=2, column=1, pady=7)

        button_create.grid(row=3, column=1, pady=15)

        def check_author():
            label_author_lastname.config(state=(NORMAL if author.get() else DISABLED))
            entry_author_lastname.config(state=(NORMAL if author.get() else DISABLED))
            label_author_firstname.config(state=(NORMAL if author.get() else DISABLED))
            entry_author_firstname.config(state=(NORMAL if author.get() else DISABLED))
            label_author_lastname.update()
            entry_author_lastname.update()
            label_author_firstname.update()
            entry_author_firstname.update()

        def create():
            msg = ""
            tmp = entry_new_login.get()
            if(tmp==""):
                msg+="- Le pseudo ne peut pas être vide\n\n"
            if(len(tmp)>20):
                msg+="- Le pseudo ne peut pas dépasser 20 caractères\n\n"
            tmp = entry_new_mail.get()
            if('@' not in tmp or '.' not in tmp):
                msg+="- Le mail n'est pas valide\n\n"
            tmp = entry_new_pwd.get()
            if(len(tmp)<7):
                msg+="- Le mot de passe doit contenir minimum 7 caractères\n\n"
            if(tmp!=entry_new_pwd_conf.get()):
                msg+="- Les mots de passe ne sont pas identiques\n\n"
            if(author.get()):
                tmp = entry_author_lastname.get()
                if(tmp == ""):
                    msg+="Vous devez entrer un nom d'auteur\n\n"
                if(len(tmp) > 30):
                    msg+="Nom d'auteur trop grand, max 30 caractères\n\n"
                tmp = entry_author_firstname.get()
                if(tmp == ""):
                    msg+="Vous devez entrer un prénom d'auteur\n\n"
                if(len(tmp) > 30):
                    msg+="prénom d'auteur trop grand, max 30 caractères\n\n"

            if (msg!=""):
                messagebox.showerror("Erreur", msg)
            else:
                warn="- Un mail de confirmation vous a été envoyé"
                if(author.get()):
                    warn+="\n\n- Une demande de validation de compte membre auteur a été envoyée à un administrateur"
                messagebox.showwarning("Compte créé", warn)
                global login
                global pwd
                login = entry_new_login.get()
                pwd = entry_new_pwd.get()
                win_con.destroy()
                con()

    def connect():
        if(entry_login.get() != login or entry_pwd.get() != pwd):
            messagebox.showerror("Erreur", "Pseudo ou mot de passe invalide")
            return
        win_con.destroy()
        frame0.destroy()
        frame1.destroy()
        frame2.destroy()
        frame3.destroy()
        win.update()
        global connected
        connected = True
        acceuil()

def profile():
    pframe0 = Frame(win)
    pframe1 = Frame(win)
    #pframe1 = LabelFrame(pframe0)

    img_canvas = Canvas()
    img_profile = Image.open("img/empty_profile.png")
    img_canvas.image = ImageTk.PhotoImage(img_profile.resize((140, 140), Image.ANTIALIAS))
    img_canvas.create_image(100, 100, image=img_canvas.image)

    #pframe0.pack()
    #pframe1.grid(row=0, column=1)
    
    img_canvas.grid()
    pframe0.pack()
    

if __name__ == "__main__":
    print("lucky world!")
    acceuil()
    win.mainloop()